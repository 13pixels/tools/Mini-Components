﻿
namespace ThirteenPixels.MiniComponents.Editor
{
    using UnityEngine;
    using UnityEditor;
    using UnityEditorInternal;
    using static UnityEditorInternal.ReorderableList;
    using System;
    using System.Linq;

    [CustomPropertyDrawer(typeof(MiniComponentContainerBase), true)]
    public class MiniComponentContainerDrawer : PropertyDrawer
    {
        private const string componentPropertyPath = "components";
        private ReorderableList list;

        private void GenerateListIfNeeded(SerializedProperty property, GUIContent label)
        {
            var componentsProperty = property.FindPropertyRelative(componentPropertyPath);

            if (list != null && list.serializedProperty.serializedObject.targetObject == property.serializedObject.targetObject) return;
            
            list = new ReorderableList(property.serializedObject,
                                       componentsProperty,
                                       true,
                                       true,
                                       true,
                                       true);

            list.drawHeaderCallback = rect => GUI.Label(rect, label);
            list.drawElementCallback = DrawElement;
            list.elementHeightCallback = index => GetElementHeight(componentsProperty, index);
            list.onAddDropdownCallback = GetAddButtonDropdown(componentsProperty);
            list.onRemoveCallback = list => DeleteElement(list, componentsProperty);
        }

        private void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            var listProperty = list.serializedProperty;
            var property = listProperty.GetArrayElementAtIndex(index);

            var headerRect = rect;
            headerRect.xMin += 14;
            headerRect.height = EditorGUIUtility.singleLineHeight;
            DrawComponentHeader(property, headerRect);

            EditorGUI.PropertyField(rect, property, GUIContent.none, true);
        }

        private static void DrawComponentHeader(SerializedProperty property, Rect rect)
        {
            var name = ((MiniComponentBase)property.managedReferenceValue).title;
            GUI.Label(rect, name, EditorStyles.boldLabel);
        }

        private float GetElementHeight(SerializedProperty listProperty, int index)
        {
            var property = listProperty.GetArrayElementAtIndex(index);
            return EditorGUI.GetPropertyHeight(property);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.FindPropertyRelative(componentPropertyPath) != null)
            {
                GenerateListIfNeeded(property, label);

                property.serializedObject.Update();
                list.DoList(position);
                property.serializedObject.ApplyModifiedProperties();
            }
            else
            {
                EditorGUI.HelpBox(position, "Your mini component base class must be [System.Serializable].", MessageType.Error);
            }
        }

        private AddDropdownCallbackDelegate GetAddButtonDropdown(SerializedProperty property)
        {
            var container = (MiniComponentContainerBase)fieldInfo.GetValue(property.serializedObject.targetObject);
            var componentBaseType = container.GetComponentBaseType();

            var menu = GetSubclassesMenu(componentBaseType, type => CreateComponent(type, property));

            return (buttonRect, list) => menu.ShowAsContext();
        }

        private void CreateComponent(Type type, SerializedProperty property)
        {
            var serializedObject = property.serializedObject;

            Undo.SetCurrentGroupName("Add Mini Component");
            serializedObject.Update();

            var index = property.arraySize;
            property.InsertArrayElementAtIndex(index);
            property = property.GetArrayElementAtIndex(index);

            var newComponent = Activator.CreateInstance(type);
            property.managedReferenceValue = newComponent;
            
            serializedObject.ApplyModifiedProperties();
            Undo.CollapseUndoOperations(Undo.GetCurrentGroup());
        }

        private static GenericMenu GetSubclassesMenu(Type baseType, Action<Type> clickHandler)
        {
            var menu = new GenericMenu();
            var subtypes = TypeCache.GetTypesDerivedFrom(baseType).Where(type => !type.IsAbstract);
            foreach (var type in subtypes)
            {
                var t = type;
                menu.AddItem(new GUIContent(type.Name), false, () => clickHandler(t));
            }
            return menu;
        }

        private void DeleteElement(ReorderableList list, SerializedProperty property)
        {
            Undo.SetCurrentGroupName("Delete Mini Component");

            var serializedObject = property.serializedObject;
            serializedObject.Update();

            property.DeleteArrayElementAtIndex(list.index);
            
            serializedObject.ApplyModifiedProperties();
            EditorUtility.SetDirty(serializedObject.targetObject);
            
            Undo.CollapseUndoOperations(Undo.GetCurrentGroup());
        }
    }
}
