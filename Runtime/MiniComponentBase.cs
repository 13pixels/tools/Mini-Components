﻿
namespace ThirteenPixels.MiniComponents
{
    [System.Serializable]
    public abstract class MiniComponentBase
    {
        public abstract string title { get; }
    }
}
