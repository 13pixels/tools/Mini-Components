﻿
namespace ThirteenPixels.MiniComponents
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public abstract class MiniComponentContainerBase<TBase> : MiniComponentContainerBase, IEnumerable<TBase>
        where TBase : MiniComponentBase
    {
        [SerializeReference]
        protected List<TBase> components;


        internal override sealed System.Type GetComponentBaseType()
        {
            return typeof(TBase);
        }

        /// <summary>
        /// Adds a component of the given type to the container and returns a reference to it.
        /// </summary>
        public T AddComponent<T>()
            where T : TBase, new()
        {
            var c = new T();
            components.Add(c);
            return c;
        }

        /// <summary>
        /// Returns the first found component of the given type form this container.
        /// </summary>
        public void RemoveComponent<T>()
            where T : TBase
        {
            foreach (var c in components)
            {
                if (c is T)
                {
                    components.Remove(c);
                    return;
                }
            }
        }

        /// <summary>
        /// Returns a reference to the component of the given type, if one is present in this container. Returns null otherwise.
        /// </summary>
        public T GetComponent<T>()
            where T : TBase
        {
            foreach (var c in components)
            {
                if (c is T)
                {
                    return (T)c;
                }
            }

            return null;
        }

        IEnumerator<TBase> IEnumerable<TBase>.GetEnumerator()
        {
            return components.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return components.GetEnumerator();
        }
    }

    [System.Serializable]
    public abstract class MiniComponentContainerBase
    {
        internal abstract System.Type GetComponentBaseType();
    }
}
